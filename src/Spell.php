<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 22/01/2019
 * Time: 14:36
 */

namespace App;


class Spell
{
  private $title, $level, $castTime,$reach,$components, $duration,$school,$isRitual = false,$description;

  /**
   * @return array
   */
  function getAsArray()
  {
    $rtn = [
      'title' => $this->getTitle(),
      'level' => $this->getLevel(),
      'school' => $this->getSchool(),
      'isRitual' => $this->isRitual(),
      'castTime' => $this->getCastTime(),
      'components' => $this->getComponents(),
      'duration' => $this->getDuration(),
      'description' => $this->getDescription()
    ];
    return $rtn;
  }

  /**
   * @return mixed
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param mixed $description
   */
  public function setDescription($description)
  {
    $this->description = $description;
  }
  public function addDescription($description)
  {
    $this->description = ($this->description==null)?$description:$this->description.$description;
  }
  /**
   * @return bool
   */
  public function isRitual()
  {
    return $this->isRitual;
  }

  /**
   * @param bool $isRitual
   */
  public function setIsRitual($isRitual)
  {
    $this->isRitual = $isRitual;
  }

  /**
   * @return mixed
   */
  public function getSchool()
  {
    return $this->school;
  }

  /**
   * @param mixed $school
   */
  public function setSchool($school)
  {
    $this->school = $school;
  }
  /**
   * @return mixed
   */
  public function getTitle()
  {
    return $this->title;
  }

  /**
   * @param mixed $title
   */
  public function setTitle($title)
  {
    $this->title = $title;
  }

  /**
   * @return mixed
   */
  public function getLevel()
  {
    return $this->level;
  }

  /**
   * @param mixed $level
   */
  public function setLevel($level)
  {
    $this->level = $level;
  }

  /**
   * @return mixed
   */
  public function getCastTime()
  {
    return $this->castTime;
  }

  /**
   * @param mixed $castTime
   */
  public function setCastTime($castTime)
  {
    $this->castTime = $castTime;
  }

  /**
   * @return mixed
   */
  public function getReach()
  {
    return $this->reach;
  }

  /**
   * @param mixed $reach
   */
  public function setRange($reach)
  {
    $this->reach = $reach;
  }

  /**
   * @return mixed
   */
  public function getComponents()
  {
    return $this->components;
  }

  /**
   * @param mixed $components
   */
  public function setComponents($components)
  {
    $this->components = $components;
  }

  /**
   * @return mixed
   */
  public function getDuration()
  {
    return $this->duration;
  }

  /**
   * @param mixed $duration
   */
  public function setDuration($duration)
  {
    $this->duration = $duration;
  }

}