<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 22/01/2019
 * Time: 14:34
 */

namespace App\Controllers;


use App\Spell;
use PHPHtmlParser\Dom;

class SpellParserController extends AbstractParserController
{
  /** @var Spell[] $spells */
  private $spells = [];
  /** @var Spell */
  private $spell;

  function processFile()
  {
    parent::processFile();
     /** @var Dom\HtmlNode $node */
    foreach ($this->contents as $node) {
      $tag = $node->getTag();
      if ($tag->name() == "h3") {
        if ($this->spell != null) {
          $this->spells[] = $this->spell;
        }
        $this->spell = new Spell();
        $this->spell->setTitle(rtrim($node->innerHtml()));
      } else {
        if ($this->spell != null) {
          $this->populateSpell($node);
        }
      }
    }
  }

  function populateSpell(Dom\HtmlNode $node)
  {
    $text = $node->innerHtml();
    if (strstr($text, 'Nivel') || stristr($text,'truco')) {
      $this->parseLevelAndSchool($node, $text);
    }
    $this->parseCastTimeComponentsAndDuration($node);
    $this->parseDescription($node);
  }

  function parseLevelAndSchool(Dom\HtmlNode $node, $text)
  {
    preg_match("/^[[a-z]*\s([0-9])]|(\w*),\s(\w*)/miu", $text, $matches);
    if (sizeof($matches)) {
      if(stristr($matches[2],'Truco')){
        $this->spell->setLevel(0);
      }else{
        $this->spell->setLevel((int) $matches[2]);
      }
      $this->spell->setSchool($matches[3]);
      if (strstr($text, 'ritual')) {
        $this->spell->setIsRitual(true);
      }
    }
  }

  function parseCastTimeComponentsAndDuration(Dom\HtmlNode $node)
  {
    /**
     * @TODO take into account spells that were not formatted properly
     */
    //Si es clase s3 seguramente es casttime, components o duration
      if ($node->hasChildren()) {
        $children = $node->getChildren();
        if (sizeof($children) > 1) {
          /** @var Dom\HtmlNode $title */
          $title = $children[0];
          /** @var Dom\HtmlNode $value */
          $value = $children[1];
          if (strstr($title, 'Tiempo')) {
            $this->spell->setCastTime($value->innerHtml());
          } elseif (strstr($title, 'Alcance')) {
            $this->spell->setRange($value->innerHtml());
          } elseif (strstr($title, 'Componentes')) {
            $this->spell->setComponents($value->innerHtml());
          } elseif (strstr($title, 'Duraci')) {
            $this->spell->setDuration($value->innerHtml());
          }
        }
      }
  }

  function parseDescription(Dom\HtmlNode $node)
  {
    //We only take into account p tags for description (for now)
    $tag = $node->getTag();
    if($tag->name() == "p" && !$tag->hasAttribute('class')){
      $this->spell->addDescription(strip_tags($node->innerHtml()));
    }
  }

  function getSpellsAsArray()
  {
    $rtn = [];
    foreach ($this->spells as $spell) {
      $rtn[] = $spell->getAsArray();
    }
    return $rtn;
  }
}