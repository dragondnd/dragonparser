<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 23/01/2019
 * Time: 17:37
 */

namespace App\Controllers;

//([A-Z][\w\s][^A-Z]*)
use App\CharacterClass;
use App\Spell;
use PHPHtmlParser\Dom\HtmlNode;

class SpellListParserController extends AbstractParserController
{

  /** @var CharacterClass[] $spells */
  private $characterClasses = [];

  /** @var CharacterClass */
  private $characterClass;

  function processFile()
  {
    parent::processFile();
    /** @var HtmlNode $node */
    foreach ($this->contents as $node) {
      if ($node->getTag()->name() == "h1") {
        if ($this->characterClass != null) {
          $this->characterClasses[] = $this->characterClass;
        }
        $this->characterClass = new CharacterClass();
        $this->characterClass->setName(rtrim($this->parseClassName($node->innerHtml())));
      } else {
        if ($this->characterClass !== null) {
          $this->populateSpellList($node);
        }
      }
    }
  }

  function populateSpellList(HtmlNode $node)
  {
    $tag = $node->getTag();
    if ($tag->name() == "p") {
      $this->parseSpellList($node);
    }
  }

  function parseSpellList(HtmlNode $node)
  {
    preg_match_all("/([A-Z][\w\s][^A-Z]*)/um", $node->innerHtml(), $matches);
    if (sizeof($matches)) {
      foreach ($matches[0] as $match) {
        $spell = new Spell();
        $spell->setTitle(rtrim($match));
        $this->characterClass->addLearnableSpell($spell);
      }
    }
  }

  function parseClassName($string)
  {
    preg_match("/(\w+)$/u",trim($string),$matches);
    return ucfirst($matches[0]);
  }

  function getCharacterClassAsArray()
  {
    $rtn = [];
    foreach ($this->characterClasses as $characterClass) {
      $rtn[] = $characterClass->getAsArray();
    }
    return $rtn;
  }

}