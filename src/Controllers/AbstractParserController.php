<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 23/01/2019
 * Time: 18:05
 */

namespace App\Controllers;


use PHPHtmlParser\Dom;

abstract class AbstractParserController
{
  /** @var Dom $dom */
  protected $dom;
  /** @var Dom\Collection $contents */
  protected $contents;
  function __construct()
  {
    $this->dom = new Dom();
  }

  function readFile($filename)
  {
    $this->dom->loadFromFile($filename);
  }
  function processFile(){
    $this->contents = $this->dom->find("body")->getChildren();
  }
}