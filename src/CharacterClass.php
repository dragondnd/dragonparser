<?php
/**
 * Created by PhpStorm.
 * User: jason
 * Date: 23/01/2019
 * Time: 18:03
 */

namespace App;


class CharacterClass
{
  private $name,$learnableSpells = [],$abilities = [];

  function getAsArray()
  {
    $rtn = [
      'name' => $this->getName(),
      'abilities' => $this->getAbilities(),
      'learnableSpells' => $this->getLearnableSpellsAsArray()
    ];
    return $rtn;
  }
  function addLearnableSpell(Spell $spell)
  {
    $this->learnableSpells[] = $spell;
  }
  function getLearnableSpellsAsArray(){
    $rtn = [];
    foreach ($this->learnableSpells as $spell) {
      $rtn[] = $spell->getAsArray();
    }
    return $rtn;
  }
  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   */
  public function setName($name)
  {
    $this->name = $name;
  }

  /**
   * @return array
   */
  public function getLearnableSpells()
  {
    return $this->learnableSpells;
  }

  /**
   * @param array $learnableSpells
   */
  public function setLearnableSpells($learnableSpells)
  {
    $this->learnableSpells = $learnableSpells;
  }

  /**
   * @return array
   */
  public function getAbilities()
  {
    return $this->abilities;
  }

  /**
   * @param array $abilities
   */
  public function setAbilities($abilities)
  {
    $this->abilities = $abilities;
  }

}