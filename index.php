<?php
//ini_set('memory_limit','500M');
require "vendor/autoload.php";
//$parser = new \App\Controllers\SpellParserController();
//$parser->readFile('data/Conjuros.html');
//$parser->processFile();
header("Content-Type: application/json");
$spellListParser = new \App\Controllers\SpellListParserController();
$spellListParser->readFile('data/ListadoDeConjuros.html');
$spellListParser->processFile();

//echo json_encode($parser->getSpellsAsArray());
echo json_encode($spellListParser->getCharacterClassAsArray());
